package com.kaspi.lab.hw_20;

import com.kaspi.lab.hw_20.entity.Employee;
import com.kaspi.lab.hw_20.service.EmployeeService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

class Result {
    private boolean ok;
    private String description;

    public Result() {
    }

    public Result(boolean ok, String description) {
        this.ok = ok;
        this.description = description;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

@Path("/employee")
public class EmployeeResource {
    @GET
    @Path("all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Employee> readAll() {
        return new EmployeeService().getAll();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Employee read(@PathParam("id") long id) {
        return new EmployeeService().get(id);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result create(Employee employee) {
        new EmployeeService().create(employee);
        return new Result(true, "Created " + employee);
    }

    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Result delete(@PathParam("id") long id) {
        new EmployeeService().delete(id);
        return new Result(true, "Deleted employee with id: " + id);
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result update(Employee employee) {
        if (employee.getId() == 0) {
            return new Result(false, "ID is not set: " + employee);
        }
        new EmployeeService().update(employee);
        return new Result(true, String.format("Updated %s", employee.toString()));
    }

}