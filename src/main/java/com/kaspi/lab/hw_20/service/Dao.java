package com.kaspi.lab.hw_20.service;

import com.kaspi.lab.hw_20.util.HibernateUtil;
import org.hibernate.Session;

import java.util.List;

public abstract class Dao<T> {
    public abstract void create(T t);
    public abstract void update(T t);
    public abstract T get(long id);
    public abstract void delete(long id);
    public abstract List<T> getAll();

    Session openSession() {
        return HibernateUtil.getSessionFactory().openSession();
    }

    void closeSession() {
        HibernateUtil.getSessionFactory().close();
    }
}
