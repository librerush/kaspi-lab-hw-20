package com.kaspi.lab.hw_20.service;

import com.kaspi.lab.hw_20.entity.Employee;
import org.hibernate.Session;

import java.util.List;

public class EmployeeService extends Dao<Employee> {
    @Override
    public void create(Employee employee) {
        Session session = openSession();
        session.beginTransaction();
        session.save(employee);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Employee employee) {
        Session session = openSession();
        session.beginTransaction();
        session.update(employee);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Employee get(long id) {
        Session session = openSession();
        Employee employee = session.get(Employee.class, id);
        session.close();
        return employee;
    }

    @Override
    public void delete(long id) {
        Session session = openSession();
        session.beginTransaction();
        Employee employee = session.get(Employee.class, id);
        session.delete(employee);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<Employee> getAll() {
        Session session = openSession();
        List<Employee> employeeList = session.createQuery("from Employee ").getResultList();
        session.close();
        return employeeList;
    }
}
